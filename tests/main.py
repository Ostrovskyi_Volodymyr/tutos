from tester.ConfigReader import ConfigReader
from tester.Tester import Tester
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run STM32 tests')
    parser.add_argument("--firmware", type=str, required=True, help="path to firmware")
    parser.add_argument("--gdb", type=str, required=True, help="path to gdb")
    parser.add_argument("--conf", type=str, required=True, help="path to configuration")
    args = parser.parse_args()
    conf = ConfigReader(args.conf)
    tester = Tester(args.gdb, args.firmware, conf.get_config())
    tester.run_tests()
    tester.get_results()
