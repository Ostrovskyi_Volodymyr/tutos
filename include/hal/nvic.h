#ifndef _HAL_NVIC_H_
#define _HAL_NVIC_H_
#ifdef __cplusplus
extern "C" {
#endif

void hal_nvic_enable_irq(unsigned int irqn);
void hal_nvic_disable_irq(unsigned int irqn);
int hal_nvic_irq_enabled(unsigned int irqn);

#ifdef __cplusplus
}
#endif
#endif  // _HAL_NVIC_H_
