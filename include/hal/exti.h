#ifndef _HAL_EXTI_H_
#define _HAL_EXTI_H_
#ifdef __cplusplus
extern "C" {
#endif

void hal_exti_enable_interrupt(unsigned int line, unsigned int trigger_edge);
void hal_exti_disable_interrupt(unsigned int line);
int hal_exti_get_pending(unsigned int line);
void hal_exti_clear_pending(unsigned int line);

#define HAL_EXTI_EDGE_RISING 0b01
#define HAL_EXTI_EDGE_FALLING 0b10

#ifdef __cplusplus
}
#endif
#endif  // _HAL_EXTI_H_
