#ifndef _HAL_RCC_H_
#define _HAL_RCC_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned int clk_freq_t;

typedef struct hal_sysclk_cfg {
    int source;
    int divider;
    union {
        struct {
            clk_freq_t freq;
        } hsi;
        struct {
            int source;
            clk_freq_t source_freq;
            int mul;
        } pll;
    };
} hal_sysclk_cfg_t;

void hal_rcc_sysclk_init(const hal_sysclk_cfg_t *cfg);
clk_freq_t hal_rcc_sysclk_freq(void);
void hal_rcc_gpio_enable(unsigned int port);
void hal_rcc_gpio_disable(unsigned int port);
int hal_rcc_gpio_enabled(unsigned int port);
void hal_rcc_afio_enable(void);
void hal_rcc_afio_disable(void);
int hal_rcc_afio_enabled(void);
void hal_rcc_uart_enable(unsigned int uart);
void hal_rcc_uart_disable(unsigned int uart);

#define HAL_SYSCLK_SRC_HSI 0b00
#define HAL_SYSCLK_SRC_HSE 0b01
#define HAL_SYSCLK_SRC_PLL 0b10

#define HAL_SYSCLK_DIV_1 0b0000
#define HAL_SYSCLK_DIV_2 0b1000

#define HAL_PLL_SRC_HSI 0b0

#define HAL_PLL_MUL_2 0b0000
#define HAL_PLL_MUL_3 0b0001
#define HAL_PLL_MUL_4 0b0010
#define HAL_PLL_MUL_5 0b0011
#define HAL_PLL_MUL_6 0b0100
#define HAL_PLL_MUL_7 0b0101
#define HAL_PLL_MUL_8 0b0110
#define HAL_PLL_MUL_9 0b0111

#ifdef __cplusplus
}
#endif
#endif  // _HAL_RCC_H_
