#ifndef _HAL_GPIO_H_
#define _HAL_GPIO_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct hal_gpio {
    struct _gpio_base *_base;
    struct _gpio_alias *_alias;
    unsigned int pin;
} hal_gpio_t;

hal_gpio_t hal_gpio_init(unsigned int port, unsigned int pin, unsigned int mode);
void hal_gpio_deinit(hal_gpio_t *hgpio);
unsigned int hal_gpio_read(hal_gpio_t *hgpio);
void hal_gpio_write(hal_gpio_t *hgpio, unsigned int v);
void hal_gpio_switch(hal_gpio_t *hgpio);

#define HAL_GPIO_PORT_A 0
#define HAL_GPIO_PORT_B 1
#define HAL_GPIO_PORT_C 2
#define HAL_GPIO_PORT_D 3
#define HAL_GPIO_PORT_E 4
#define HAL_GPIO_PORT_F 5
#define HAL_GPIO_PORT_G 6

#define HAL_GPIO_OUT_SPEED_LOW 0b0010
#define HAL_GPIO_OUT_SPEED_MID 0b0001
#define HAL_GPIO_OUT_SPEED_HIGH 0b0011
#define HAL_GPIO_MODE_OUT_PP 0b0000
#define HAL_GPIO_MODE_OUT_OD 0b0100
#define HAL_GPIO_MODE_ALT_PP 0b1000
#define HAL_GPIO_MODE_ALT_OD 0b1100
#define HAL_GPIO_MODE_INP_AG 0b0000
#define HAL_GPIO_MODE_INP_FL 0b0100
#define HAL_GPIO_MODE_INP_PP 0b1000
#define HAL_GPIO_MODE_EXTI 0b10000

#ifdef __cplusplus
}
#endif
#endif  // _HAL_GPIO_H_
