#ifndef _HAL_SYSTICK_H_
#define _HAL_SYSTICK_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    unsigned char intr_enable;
    int source;
    unsigned int value;
} systick_cfg_t;

void hal_systick_init(systick_cfg_t cfg);
void hal_systick_enable(void);
void hal_systick_disable(void);
void hal_systick_set(unsigned int value);
unsigned int hal_systick_get(void);
void hal_systick_restart(void);

#define HAL_SYSTICK_SRC_AHB_DIV_8 0x0
#define HAL_SYSTICK_SRC_AHB 0x1

#ifdef __cplusplus
}
#endif
#endif  // _HAL_SYSTICK_H_
