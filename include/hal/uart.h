#ifndef _HAL_UART_H_
#define _HAL_UART_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct hal_uart {
    struct _uart_base *_base;
    struct _uart_alias *_alias;
} hal_uart_t;

typedef struct hal_uart_cfg {
    unsigned int uart;
    unsigned int mode;
    unsigned int word_len;
    unsigned int baud_rate;
} hal_uart_cfg_t;

hal_uart_t hal_uart_init(hal_uart_cfg_t cfg);
void hal_uart_transmit(hal_uart_t *uart, const unsigned char *buf, unsigned int size);
void hal_uart_receive(hal_uart_t *uart, unsigned char *buf, unsigned int size);

#define HAL_UART_2 0
#define HAL_UART_3 1
#define HAL_UART_4 2
#define HAL_UART_5 3

#define HAL_UART_MODE_TX 0b01
#define HAL_UART_MODE_RX 0b10

#define HAL_UART_WORD_LEN_8 0b0
#define HAL_UART_WORD_LEN_9 0b1

#ifdef __cplusplus
}
#endif
#endif  // _HAL_UART_H_
