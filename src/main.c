#include <hal/exti.h>
#include <hal/gpio.h>
#include <hal/rcc.h>

hal_gpio_t led_pin;
hal_gpio_t bt_pin;

void main(void) {
    hal_rcc_sysclk_source(HAL_SYSCLK_SRC_HSI);
    hal_rcc_gpio_enable(HAL_GPIO_PORT_A);
    hal_rcc_gpio_enable(HAL_GPIO_PORT_B);
    hal_rcc_afio_enable();

    led_pin = hal_gpio_init(HAL_GPIO_PORT_B, 10, HAL_GPIO_MODE_OUT_PP | HAL_GPIO_OUT_SPEED_LOW);
    bt_pin = hal_gpio_init(HAL_GPIO_PORT_A, 2, HAL_GPIO_MODE_INP_PP | HAL_GPIO_MODE_EXTI);
    hal_gpio_write(&led_pin, 0);
    hal_gpio_write(&bt_pin, 0);

    hal_exti_enable_interrupt(2, HAL_EXTI_EDGE_RISING);

    while (1) {
    }
}

void irq_exti2_handler(void) {
    for (int i = 0; i < 1000; ++i) {
    }

    if (hal_gpio_read(&bt_pin))
        hal_gpio_switch(&led_pin);
    hal_exti_clear_pending(2);
}
