extern void *_estack, *_sidata, *_sdata, *_edata, *_sbss, *_ebss;

void main(void);

void __attribute__((naked, noreturn)) irq_reset_handler(void) {
    void **data_in_flash, **ram;

    for (data_in_flash = &_sidata, ram = &_sdata; ram != &_edata; data_in_flash++, ram++) {
        *ram = *data_in_flash;
    }

    for (ram = &_sbss; ram != &_ebss; ram++) {
        *ram = 0;
    }
    main();
    while (1) {
    }
}

void __attribute__((naked, noreturn)) irq_default_handler() {
    while (1) {
    }
}

#define WEAK_INT_HANDLER(funcname) \
    void funcname() __attribute__((weak, alias("irq_default_handler")));

WEAK_INT_HANDLER(irq_systick_handler)
WEAK_INT_HANDLER(irq_exti0_handler)
WEAK_INT_HANDLER(irq_exti1_handler)
WEAK_INT_HANDLER(irq_exti2_handler)
WEAK_INT_HANDLER(irq_exti3_handler)
WEAK_INT_HANDLER(irq_exti4_handler)
WEAK_INT_HANDLER(irq_exti5_9_handler)
WEAK_INT_HANDLER(irq_exti10_15_handler)

// clang-format off
void *vectors[] __attribute__((section(".isr_vector"), used)) = {
    &_estack,
    &irq_reset_handler,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0, 0,
    &irq_systick_handler,
	0, 0, 0,
	0, 0, 0,
	&irq_exti0_handler,
	&irq_exti1_handler,
	&irq_exti2_handler,
	&irq_exti3_handler,
	&irq_exti4_handler,
};
// clang-format on
