#include <hal/common.h>
#include <hal/gpio.h>

struct _gpio_base {
    unsigned int crl;
    unsigned int crh;
    unsigned int id;
    unsigned int od;
    unsigned int bsr;
    unsigned int br;
    unsigned int lck;
};

struct _gpio_alias {
    struct {
        unsigned int mode[2];
        unsigned int cnf[2];
    } moder[16];
    unsigned int id[32];
    unsigned int od[32];
    unsigned int bsr[32];
    unsigned int br[32];
    unsigned int lck[32];
};

struct _afio_base {
    unsigned int evc;
    unsigned int map1;
    unsigned int exti[4];
    unsigned int map2;
} *afio_base = (struct _afio_base *)AFIO_BASE_ADDR;

static void _hal_gpio_init(hal_gpio_t *hgpio, unsigned int mode);
static void _hal_afio_it_init(hal_gpio_t *hgpio, unsigned int port);

hal_gpio_t hal_gpio_init(unsigned int port, unsigned int pin, unsigned int mode) {
    hal_gpio_t hgpio;
    hgpio.pin = pin;
    hgpio._base = (struct _gpio_base *)(GPIO_BASE_ADDR + GPIO_OFFSET * port);
    hgpio._alias = (struct _gpio_alias *)PERIF_ALIAS_ADDR(GPIO_BASE_ADDR + GPIO_OFFSET * port);

    if (mode & HAL_GPIO_MODE_EXTI) {
        _hal_afio_it_init(&hgpio, port);
        mode &= 0xF;
    }
    _hal_gpio_init(&hgpio, mode);

    return hgpio;
}

void hal_gpio_deinit(hal_gpio_t *hgpio) {
    unsigned int moder_pos = (hgpio->pin < 8 ? hgpio->pin : hgpio->pin - 8) * 4;
    unsigned int *cfg_reg = (hgpio->pin < 8 ? &hgpio->_base->crl : &hgpio->_base->crh);
    unsigned int exti_reg = hgpio->pin / 4;
    unsigned int line_pos = (hgpio->pin - exti_reg * 4) * 4;

    *cfg_reg &= ~(0b1111 << moder_pos);
    *cfg_reg |= HAL_GPIO_MODE_INP_FL << moder_pos;
    afio_base->exti[exti_reg] &= ~(0b1111 << line_pos);
}

unsigned int hal_gpio_read(hal_gpio_t *hgpio) {
    unsigned int pin = hgpio->pin;
    return hgpio->_alias->id[pin];
}

void hal_gpio_write(hal_gpio_t *hgpio, unsigned int v) {
    unsigned int pin = hgpio->pin;
    hgpio->_alias->od[pin] = (v == 0 ? 0 : 1);
}

void hal_gpio_switch(hal_gpio_t *hgpio) {
    unsigned int v = hal_gpio_read(hgpio);
    hal_gpio_write(hgpio, !v);
}

static void _hal_gpio_init(hal_gpio_t *hgpio, unsigned int mode) {
    unsigned int moder_pos = (hgpio->pin < 8 ? hgpio->pin : hgpio->pin - 8) * 4;
    unsigned int *cfg_reg = (hgpio->pin < 8 ? &hgpio->_base->crl : &hgpio->_base->crh);

    *cfg_reg &= ~(0b1111 << moder_pos);
    *cfg_reg |= mode << moder_pos;
}

static void _hal_afio_it_init(hal_gpio_t *hgpio, unsigned int port) {
    unsigned int exti_reg = hgpio->pin / 4;
    unsigned int line_pos = (hgpio->pin - exti_reg * 4) * 4;

    afio_base->exti[exti_reg] &= ~(0b1111 << line_pos);
    afio_base->exti[exti_reg] |= port << line_pos;
}
