#include <hal/common.h>
#include <hal/rcc.h>

struct _rcc_base {
    unsigned int ctrl;
    unsigned int cfg;
    unsigned int ci;
    unsigned int apb2rst;
    unsigned int apb1rst;
    unsigned int ahben;
    unsigned int apb2en;
    unsigned int apb1en;
    unsigned int bdc;
    unsigned int cs;
} *rcc_base = (struct _rcc_base *)(RCC_BASE_ADDR);

struct _rcc_alias {
    unsigned int ctrl[32];
    unsigned int cfg[32];
    unsigned int ci[32];
    unsigned int apb2rst[32];
    unsigned int apb1rst[32];
    unsigned int ahben[32];
    unsigned int apb2en[32];
    unsigned int apb1en[32];
    unsigned int bdc[32];
    unsigned int cs[32];
} *rcc_alias = (struct _rcc_alias *)PERIF_ALIAS_ADDR(RCC_BASE_ADDR);

static clk_freq_t sysclk_freq;
static clk_freq_t _sysclk_configure_hsi(const hal_sysclk_cfg_t *cfg);
static clk_freq_t _sysclk_configure_pll(const hal_sysclk_cfg_t *cfg);

void hal_rcc_sysclk_init(const hal_sysclk_cfg_t *cfg) {
    sysclk_freq = 0;

    switch (cfg->source) {
        case HAL_SYSCLK_SRC_HSI:
            sysclk_freq = _sysclk_configure_hsi(cfg);
            break;
        case HAL_SYSCLK_SRC_PLL:
            sysclk_freq = _sysclk_configure_pll(cfg);
            break;
        default:
            return;
    }

    if (cfg->divider != HAL_SYSCLK_DIV_1) {
        sysclk_freq /= 1 << (cfg->divider - 0b0111);
    }

    rcc_base->cfg &= ~(0b1111 << 4);
    rcc_base->cfg |= cfg->divider << 4;

    rcc_base->cfg &= ~0b11;
    rcc_base->cfg |= cfg->source & 0b11;
    while (rcc_base->cfg & 0b1100 != (cfg->source & 0b11) << 2) {
    }
}

clk_freq_t hal_rcc_sysclk_freq(void) {
    return sysclk_freq;
}

void hal_rcc_gpio_enable(unsigned int port) {
    rcc_alias->apb2en[port + 2] = 1;
}

void hal_rcc_gpio_disable(unsigned int port) {
    rcc_alias->apb2en[port + 2] = 0;
}

int hal_rcc_gpio_enabled(unsigned int port) {
    return rcc_alias->apb2en[port + 2];
}

void hal_rcc_afio_enable(void) {
    rcc_alias->apb2en[0] = 1;
}

void hal_rcc_afio_disable(void) {
    rcc_alias->apb2en[0] = 0;
}

int hal_rcc_afio_enabled(void) {
    return rcc_alias->apb2en[0];
}

void hal_rcc_uart_enable(unsigned int uart) {
    rcc_alias->apb1en[uart + 17] = 1;
}

void hal_rcc_uart_disable(unsigned int uart) {
    rcc_alias->apb1en[uart + 17] = 0;
}

static clk_freq_t _sysclk_configure_hsi(const hal_sysclk_cfg_t *cfg) {
    rcc_alias->cfg[0] = 1;

    return cfg->hsi.freq;
}

static clk_freq_t _sysclk_configure_pll(const hal_sysclk_cfg_t *cfg) {
    clk_freq_t pll_freq = cfg->pll.source_freq;

    if (cfg->pll.source == HAL_PLL_SRC_HSI) {
        pll_freq /= 2;
    } else {  // TODO: HSE source
    }

    rcc_alias->cfg[16] = cfg->pll.source;

    pll_freq *= cfg->pll.mul + 2;
    rcc_base->cfg &= ~(0b1111 << 18);
    rcc_base->cfg |= cfg->pll.mul << 18;

    rcc_alias->ctrl[24] = 1;
    while (!rcc_alias->ctrl[25]) {
    }

    return pll_freq;
}
