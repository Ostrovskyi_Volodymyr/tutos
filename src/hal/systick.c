#include <hal/common.h>
#include <hal/systick.h>

struct _systick_base {
    unsigned int ctrl;
    unsigned int reload;
    unsigned int current;
    unsigned int calib;
} *systick_base = (struct _systick_base *)SYSTICK_BASE_ADDR;

void hal_systick_init(systick_cfg_t cfg) {
    hal_systick_set(cfg.value);
    if (cfg.intr_enable) {
        systick_base->ctrl |= 0b10;
    }
    systick_base->ctrl |= cfg.source << 2;
}

void hal_systick_enable(void) {
    systick_base->ctrl |= 1;
}

void hal_systick_disable(void) {
    systick_base->ctrl &= ~1;
}

void hal_systick_set(unsigned int value) {
    systick_base->reload = value & 0xFFFFFF;
    hal_systick_restart();
}

unsigned int hal_systick_get(void) {
    return systick_base->current;
}

void hal_systick_restart(void) {
	systick_base->current = 0;
}