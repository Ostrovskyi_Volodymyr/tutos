#include <hal/common.h>
#include <hal/rcc.h>
#include <hal/uart.h>

struct _uart_base {
    unsigned int st;
    unsigned int data;
    unsigned int br;
    unsigned int ctrl1;
    unsigned int ctrl2;
    unsigned int ctrl3;
    unsigned int gtp;
};

struct _uart_alias {
    unsigned int st[32];
    unsigned int data[32];
    unsigned int br[32];
    unsigned int ctrl1[32];
    unsigned int ctrl2[32];
    unsigned int ctrl3[32];
    unsigned int gtp[32];
};

static unsigned int _calc_uart_divider(unsigned int freq, unsigned int baud_rate);

hal_uart_t hal_uart_init(hal_uart_cfg_t cfg) {
    unsigned int freq = 0;
    hal_uart_t uart;

    uart._base = (struct _uart_base *)(UART_BASE_ADDR + UART_OFFSET * cfg.uart);
    uart._alias = (struct _uart_alias *)PERIF_ALIAS_ADDR(UART_BASE_ADDR + UART_OFFSET * cfg.uart);

    uart._alias->ctrl1[12] = cfg.word_len;
    freq = hal_rcc_sysclk_freq();  // TODO: Get APB1/APB2 freq
    uart._base->br = _calc_uart_divider(freq, cfg.baud_rate);

    if (cfg.mode & HAL_UART_MODE_TX)
        uart._alias->ctrl1[3] = 1;
    if (cfg.mode & HAL_UART_MODE_RX)
        uart._alias->ctrl1[2] = 1;

    // Enable
    uart._alias->ctrl1[13] = 1;

    return uart;
}

void hal_uart_transmit(hal_uart_t *uart, const unsigned char *buf, unsigned int size) {
    unsigned int i = 0;

    for (; i < size; ++i) {
        while (!uart->_alias->st[7]) {
        }

        uart->_base->data = buf[i];
    }

    while (!uart->_alias->st[6]) {
    }
}

void hal_uart_receive(hal_uart_t *uart, unsigned char *buf, unsigned int size) {
    unsigned int i = 0;

    for (; i < size; ++i) {
        while (!uart->_alias->st[5]) {
        }

        buf[i] = uart->_base->data;
    }
}

unsigned int _calc_uart_divider(unsigned int freq, unsigned int baud_rate) {
    unsigned int r = (freq << 4) / (baud_rate >> 4);
    unsigned int div = r >> 8;

    return div + ((r & 0xF0) > 0x80 ? 1 : 0);
}
