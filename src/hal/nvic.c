#include <hal/common.h>
#include <hal/nvic.h>

struct _nvic_base {
    unsigned int ise[8];
    unsigned int _f[24];
    unsigned int ice[8];
} *nvic_base = (struct _nvic_base *)(NVIC_BASE_ADDR);

void hal_nvic_enable_irq(unsigned int irqn) {
    unsigned int ise_idx = irqn / 32;
    unsigned int irq_pos = irqn - ise_idx * 32;

    nvic_base->ise[ise_idx] |= 1 << irq_pos;
}

void hal_nvic_disable_irq(unsigned int irqn) {
    unsigned int ice_idx = irqn / 32;
    unsigned int irq_pos = irqn - ice_idx * 32;

    nvic_base->ice[ice_idx] |= 1 << irq_pos;
}

int hal_nvic_irq_enabled(unsigned int irqn) {
    unsigned int ise_idx = irqn / 32;
    unsigned int irq_pos = irqn - ise_idx * 32;

    return nvic_base->ise[ise_idx] & (1 << irq_pos);
}
