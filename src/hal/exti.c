#include <hal/common.h>
#include <hal/exti.h>
#include <hal/nvic.h>

#define NVIC_IRQN_LINE0_4 6
#define NVIC_IRQN_LINE5_9 23
#define NVIC_IRQN_LINE10_15 40

struct _exti_alias {
    unsigned int im[32];
    unsigned int em[32];
    unsigned int rts[32];
    unsigned int fts[32];
    unsigned int swie[32];
    unsigned int pnd[32];
} *exti_alias = (struct _exti_alias *)PERIF_ALIAS_ADDR(EXTI_BASE_ADDR);

static int _get_nvic_irqn_by_line(unsigned int line);

void hal_exti_enable_interrupt(unsigned int line, unsigned int trigger_edge) {
    if (trigger_edge & HAL_EXTI_EDGE_RISING)
        exti_alias->rts[line] = 1;
    if (trigger_edge & HAL_EXTI_EDGE_FALLING)
        exti_alias->fts[line] = 1;

    hal_nvic_enable_irq(_get_nvic_irqn_by_line(line));
    exti_alias->im[line] = 1;
}

void hal_exti_disable_interrupt(unsigned int line) {
    exti_alias->im[line] = 0;
    exti_alias->rts[line] = 0;
    exti_alias->fts[line] = 0;
}

int hal_exti_get_pending(unsigned int line) {
    return exti_alias->pnd[line];
}

void hal_exti_clear_pending(unsigned int line) {
    exti_alias->pnd[line] = 1;
}

static int _get_nvic_irqn_by_line(unsigned int line) {
    if (line <= 4)
        return NVIC_IRQN_LINE0_4 + line;
    else if (line <= 9)
        return NVIC_IRQN_LINE5_9;

    return NVIC_IRQN_LINE10_15;
}
